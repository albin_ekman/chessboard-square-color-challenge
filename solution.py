

def get_color(column, row):
    ary = ["a", "b", "c", "d", "e", "f", "g", "h"]
    x = ary.index(column) + 1

    if (x % 2) == 0:
        if ( row % 2) == 0:
            return "black"
        else:
            return "white"
    else:
        if ( row % 2 ) == 0:
            return "white"
        else:   
            return "black"
