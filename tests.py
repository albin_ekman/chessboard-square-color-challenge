from solution import get_color


def test_a_8():
    assert get_color("a", 8) == "white"


def test_b_2():
    assert get_color("b", 2) == "black"


def test_f_5():
    assert get_color("f", 5) == "white"


def test_d_6():
    assert get_color("d", 6) == "black"


def test_g_1():
    assert get_color("g", 1) == "black"


def test_g_2():
    assert get_color("g", 2) == "white"


def test_d_5():
    assert get_color("d", 5) == "white"


def test_c_7():
    assert get_color("c", 7) == "black"


def test_a_6():
    assert get_color("a", 6) == "white"


def test_e_3():
    assert get_color("e", 3) == "black"
