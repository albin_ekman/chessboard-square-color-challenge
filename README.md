### Chessboard color challenge

In this challenge, your task is to create a program which, given the location of
a square on a chessboard(i.e "c", 3) returns the color of the field underneath.

**Example outputs:**

```
"c", 3 = "black"
```

```
"b", 5 = "white"
```

```
"f", 6 = "black"
```

```
"h", 4 = "black"
```

![](https://previews.123rf.com/images/kup1984/kup19841607/kup1984160700082/59730769-chessboard-with-chess-figures-and-marking-vector-illustration-.jpg "Chessboard")


Look at the tests.py file for more.